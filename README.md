                **Newyork Parking Ticket Analysis**
                
We use NYC parking ticket data from the year 2014 to 2020 and make an analysis of it to find out some of the important details of the parking violations that leads to NYC revenue. Some of the problem statements are finding  
*    Top 3 violations that are generating the max revenue in the city of New York.
*    The top 10 vehicles that are have the most number of violation tickets.
*    The number of parking tickets issued with each violation code
*    The number of tickets issued for each plate type or vehicle make                

Used Mapreduce, Hive and Spark for processing the data and solving the problem statements.
Data & jar is uploaded to S3 bucket. S3 bucket is also used to hold the output of the results.
EMR clusters will be used to process the data using hadoop, hive and spark.
The data being used is available in https://drive.google.com/open?id=1mQT0DnaUivaM2hbIelAoaqWlbiiHEM5A (*Gitlab has a restriction of upload file size to be 10MB*) 


**HADOOP**: 
    Application is written in java.
    Driver program defines the Job parameters such as input path and output path, defines mapper class and reducer class and also invokes the job.
    Mapper class analyses the csv file from AWS s3 bucket and splits values based on delimiter and gets the column 'violation code'.
    Reducer class gets the output from mapper class and finds the frequency of occurences of 'violation code'.

    AWS EMR CLI COMMANDS:
    hadoop jar ticketanalysis.jar stubs.ViolationAnalysisDriver s3://bigdata-1067764/*.csv s3://bigdata-1067764/OUTPUT/MapReduce/ticket_op

    Result:
    Gets the total number of violations committed per violation code from year 2014 to 2020.
    
    Output Screenshots:
    https://gitlab.com/k92.suchitra/cpsc-651-big-data/tree/master/MR_ParkingTicketAnalysis/mapreduce_screenshots

**HIVE**:
    CREATE_TABLES.q contains queries to create external tables in hive shell for 'ticket data' and 'FINE'.
    Q1.q contains query to analyse performance.
    Q2.q contains query to find maximum revenue generated with w.r.t each violation code.

    AWS EMR CLI COMMANDS:
    hive
    -->queries

    Result:
    Q1.q produces total number of violations committed per violation code from year 2014 to 2020.
    Q2.q produces maximum revenue generated with w.r.t each violation code from year 2014 to 2020.
    
    Output Screenshots:
    Q1.q --> https://gitlab.com/k92.suchitra/cpsc-651-big-data/tree/master/Hive_performance/hive_performance_screenshots
    Q2.q --> https://gitlab.com/k92.suchitra/cpsc-651-big-data/tree/master/Hive/hive_screenshots

**SPARK**:
    Applications are written in java.
    Apache spark sql package is used read the data from all the csv files from AWS s3 bucket.
    IndividualIDViolation class defines code to determine the violations committed by each plate type and outputs the result to AWS s3 .
    VehicleTypeViolation class is used for performance analysis and defines code to determine violations committed for each violation type.

    AWS EMR CLI COMMANDS:
    aws s3 cp s3://bigdata-1067764/SPARK/IndividualIDViolation.jar .
    spark-submit --class stubs.IndividualIDViolation ./IndividualIDViolation.jar

    aws s3 cp s3://bigdata-1067764/SPARK/VehicleTypeViolation_Perf.jar .
    spark-submit --class stubs.VehicleTypeViolation ./VehicleTypeViolation_Perf.jar

    Result:
    IndividualIDViolation class outputs which plate types commit maximum violation from year 2014 to 2020..
    VehicleTypeViolation class outputs total number of violations committed per violation code from year 2014 to 2020.
    
    Output Screenshots:
    IndividualIDViolation --> https://gitlab.com/k92.suchitra/cpsc-651-big-data/tree/master/Spark/spark_screenshots
    VehicleTypeViolation  --> https://gitlab.com/k92.suchitra/cpsc-651-big-data/tree/master/Spark_performance
    
**PERFORMANCE**:
Problem Statement: Find total number of violations committed per violation code

         | Job    |Time(sec)|
         | ------ | ------- |
         | HIVE   | 180.107 |
         | HADOOP | 608     |
         | SPARK  | 80      |

        
        According to the results Spark is the most efficient followed by Hadoop and Hive.
    

